# @fnet/dir-to-yaml

At a high level, the "@fnet/dir-to-yaml" project's main function is to generate YAML (YAML Ain't Markup Language) content, based on the configuration and content of files within a specific directory of a system, and on matching specific file patterns within this directory. The generated YAML content depicts the directory's file hierarchy and the content of the matched files.

## Functionality

The "@fnet/dir-to-yaml" module, as manifested in the main function, executes several significant actions:

1. **Scans a Specific Directory:** Based on user input parameters, it scans a designated directory for files matching certain specified patterns. This is made possible using the "fnetListFiles" function, which fetches a list of matching file paths.

2. **Reads and Transforms File Contents:** For each of the files matching the specified pattern, it reads the file content and transforms it into a hierarchical structure. This structure maps the relative path of the files (in relation to the specified directory) to their respective content.

3. **Creates YAML Content:** The hierarchical structure is then converted into YAML format using the "yaml.stringify" method.

4. **Copies Result to Clipboard (Optional):** If requested by the user input parameters, the resulting YAML content can be copied to the system's clipboard utilizing the package 'clipboardy'.

5. **Saves Result to File (Optional):** Alternatively (or in addition to the clipboard option), if a user specifies an output file path, the YAML content is written out to this file.

The module returns an object containing the generated YAML content, and optionally, the output file path (if provided). Basic validation of the input parameters is included in the feature set of the module, validating both the directory path and pattern input to help ensure accurate and useful outcomes.