import fs from 'node:fs';
import path from 'node:path';
import clipboardy from 'clipboardy';
import * as yaml from 'yaml';
import fnetListFiles from '@fnet/list-files';

/**
 * Scans the directory for files matching the specified patterns.
 * Generates YAML content with a file hierarchy and contents of the matched files.
 * Copies the YAML content to clipboard if specified.
 * Writes the YAML content to a file if an output path is provided.
 * Includes basic validation for input parameters.
 * Returns an object containing the YAML content and, optionally, the path to the output file.
 * 
 * @param {Object} args - The arguments for the function.
 * @param {string} args.dir - The directory to scan.
 * @param {string | string[]} args.pattern - The glob patterns to match files against.
 * @param {boolean} [args.clipboard=false] - Determines whether to copy the output to the clipboard.
 * @param {string} [args.output] - The file path where the YAML content will be written.
 * @returns {Promise<{content: string, output?: string}>} - An object containing the YAML content and optionally the output file path.
 */
export default async (args) => {
  const { dir, pattern, clipboard = false, output } = args;

  // Validates the directory path
  if (!fs.existsSync(dir)) {
    throw new Error(`The specified directory '${dir}' does not exist.`);
  }

  // Validates the pattern input
  if (!pattern || (Array.isArray(pattern) && pattern.length === 0)) {
    throw new Error(`A valid pattern must be provided.`);
  }

  try {
    // Scans the directory for files matching the specified patterns
    const filePaths = await fnetListFiles({ dir, pattern });

    // Validates that files were found
    if (filePaths.length === 0) {
      throw new Error(`No files matched the pattern '${pattern}' in directory '${dir}'.`);
    }

    // Prepares file data for YAML conversion
    const fileData = filePaths.reduce((acc, filePath) => {
      const relativePath = path.relative(dir, filePath);
      acc[relativePath] = {
        content: fs.readFileSync(filePath, 'utf8')
      };
      return acc;
    }, {});

    // Generates YAML content
    const yamlContent = yaml.stringify(fileData);

    // Copies the YAML content to clipboard if specified
    if (clipboard) clipboardy.writeSync(yamlContent);

    const result = { content: yamlContent };

    // Writes the YAML content to a file if an output path is provided
    if (output) {
      const outputPath = path.resolve(dir, output);
      fs.writeFileSync(outputPath, yamlContent, 'utf8');
      result.output = outputPath; // Adds the output file path to the result
    }

    // Returns the result object containing the YAML content and, optionally, the output file path
    return result;
  } catch (error) {
    console.error('Error processing files:', error);
    throw error; // Rethrow the error to make it clear that the operation failed
  }
};